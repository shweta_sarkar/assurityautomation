Feature: Get Categories 

Scenario Outline:1. Verify name of the category
	Given I create request for v1 categories 
	And I add parameter as
	|param|value|
	|category|false|
	And I call the GET v1 categories with category number <categoryNumber> and url <url>
	Then I see category name as <categoryName>
Examples:
|categoryNumber|url|categoryName|
|6327|"/{categoryNumber}/Details.json"|"Carbon credits"|

Scenario Outline: 2. Verify if category can be relisted 
	Given I create request for v1 categories 
	And I add parameter as 
		|param|value|
		|category|false|
	And I call the GET v1 categories with category number <categoryNumber> and url <url> 
	Then I see category can be relisted <status> 
	Examples: 
		|categoryNumber|url|status|
		|6327|"/{categoryNumber}/Details.json"|true|
		
Scenario Outline:
3.Verify that promotions element with Name = "Gallery" has a Description that contains the text "2x larger image" 
	Given I create request for v1 categories 
	And I add parameter as 
		|param|value|
		|category|false|
	And I call the GET v1 categories with category number <categoryNumber> and url <url> 
	Then I see a promotion <promotion> is present with description containing <value> 
	
	Examples: 
		|categoryNumber|url|promotion|value|
		|6327|"/{categoryNumber}/Details.json"|Gallery|"2x larger image"|