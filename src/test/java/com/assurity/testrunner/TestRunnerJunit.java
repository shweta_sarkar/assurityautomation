package com.assurity.testrunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(strict = true, features = "features/automationTestCases.feature", glue = {
		"com.assurity" }, plugin = {
				"de.monochromata.cucumber.report.PrettyReports:target/cucumber" }, monochrome = true)

public class TestRunnerJunit {

}
