package com.assurity.utilities;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks {

	@Before()
	public void setup() {
		// database connection setup
		// anything before scenario
	}

	@After(order = 0)
	public void afterScenario() {
		System.setProperty("cucumber.reporting.config.file",
				"src/test/resources/configs/cucumber-reporting.properties");
	}
}
