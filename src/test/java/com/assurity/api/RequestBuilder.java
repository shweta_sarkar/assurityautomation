package com.assurity.api;

import com.assurity.manager.FileReaderManager;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public final class RequestBuilder {
	RequestSpecBuilder requestBuilder;
	RequestSpecification request;

	public RequestBuilder() {
		requestBuilder = new RequestSpecBuilder();
		requestBuilder.setBaseUri(FileReaderManager.getInstance().getConfigReader().getBaseUri());
	}

	public RequestSpecification getRequestSpecification(RequestSpecBuilder builder) {
		return RestAssured.given(builder.build());
	}

	public RequestSpecification getRequestSpecification() {
		return RestAssured.given(requestBuilder.build());
	}
	
	// Created a method in case base uri needs to be parameterised based on environment
	@SuppressWarnings("unused")
	private String getEnvironment( ) {
		return System.getProperty("env");
	}
}
