package com.assurity.api.configuration;

public class CategoryApiConfiguration extends BaseConfiguration {
	private String categoryApiUrl = "/v1/Categories";
	public String getUri() {
		return this.categoryApiUrl;
	}
}
