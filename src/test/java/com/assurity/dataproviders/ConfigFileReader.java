package com.assurity.dataproviders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {

	
	private Properties properties;
	private final String propertyFilepath= "src/test/resources/configs/Configuration.properties";
	
	public ConfigFileReader()
	{
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilepath));
			properties= new Properties();
			try {
				properties.load(reader);
				reader.close();
			}catch (IOException e) {
				e.printStackTrace();
			}
		}catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("configuration.properties not found at "+propertyFilepath);
		}
		
	}
	
	public String getBaseUri()
	{
		String value = properties.getProperty("baseUri");
		if(value != null)
			return value;
		else throw new RuntimeException("baseUri is not mentioned in the Configuraion.properties file");
	}
}
