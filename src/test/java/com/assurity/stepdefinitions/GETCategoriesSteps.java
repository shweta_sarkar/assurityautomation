package com.assurity.stepdefinitions;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItems;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.assurity.api.RequestBuilder;
import com.assurity.api.configuration.BaseConfiguration;
import com.assurity.api.configuration.CategoryApiConfiguration;
import com.assurity.utilities.Log;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

public class GETCategoriesSteps {
	RequestSpecification request;
	BaseConfiguration apiConfig;
	ResponseOptions<Response> response;
	static Logger log = LogManager.getLogger(GETCategoriesSteps.class.getName());

	public GETCategoriesSteps(RequestBuilder _request) {
		request = _request.getRequestSpecification();
		apiConfig = new CategoryApiConfiguration();
	}

	@Given("I create request for v1 categories")
	public void i_perform_GET_operation_for_v1_categories_for_category_number() {
		request = request.basePath(apiConfig.getUri());

	}

	@Given("I add parameter as")
	public void i_add_parameter_as(DataTable dataTable) {
		for (Map<String, String> row : dataTable.asMaps()) {
			request.param(row.get("param"), row.get("value"));
		}
	}

	@Given("I call the GET v1 categories with category number {int} and url {string}")
	public void i_call_the_GET_api_with_url(Integer category, String url) {
		response = request.get(url, category);
		assertThat(response.statusCode(), is(200));
	}

	@Then("I see category name as {string}")
	public void i_see_category_name(String name) {
		assertThat(response.body().jsonPath().get("Name"), is(name));
	}

	@Then("I see category can be relisted {word}")
	public void i_see_category_can_be_relisted(String status) {
		assertThat(response.body().jsonPath().get("CanRelist"), is(Boolean.valueOf(status)));
	}

	@Then("I see a promotion {word} is present with description containing {string}")
	public void i_see_promotion_Gallery_is_present_with_description_containing(String promotion, String value)
			throws Exception {
		boolean flag = false;
		response.thenReturn().then().body("Promotions.Name", hasItems("Gallery"));
		List<Map<String, String>> list = response.body().jsonPath().getList("Promotions");
		for (Map<String, String> map : list) {
			if (map.get("Name").equals(promotion)) {
				assertThat(map.get("Description"), containsString(value));
				flag = true;
				break;
			}
		}
		if (!flag) {
			String error = "Promotion " + promotion + " not found";
			Log.logError(log, error);
			throw new Exception(error);
		}
	}
}
