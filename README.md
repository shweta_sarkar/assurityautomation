# test-automation-assurity

API automation scenarios for demo API(Assurity).


As per the requirements, this project focuses on API automation testing.

**Technology stack**: Java and Rest assured for API, Cucumber for BDD and reporting, Maven for build management, Junit for testing, Log4J for logging.


**Software versions:**

Eclipse IDE: Version: Oxygen.3a Release (4.7.3a)
OS: Windows 10

**Setup**:

Clone the project from the git repository.
The project can be imported into eclipse as a maven project. All dependencies present in pom.xml will get downloaded by maven.

**Feature File**
All the test BDD scenarios are present in automationTestCases.feature file located in "features" folder.

**Execution**:

Method 1): Tests can be executed once the project is imported into the IDE(in this case eclipse). Run the TestRunnerJunit.java class present in com.assurity.testrunner package.

OR

Method 2): It can be run from cmd prompt or terminal using the command(From project root directory)
"mvn test"
Note: Maven should be installed and path should be set up in order to run scripts from cmd/terminal


**Reporting**:

Reporting is done using a customer cucumber plugin(https://gitlab.com/monochromata-de/cucumber-reporting-plugin).
Reports are generated in "taget/cucumber/cucumber-html-reports" folder with name "report-feature-xxxx"


The default logging level is INFO which can be changed in log4j.xml